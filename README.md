To run this appication, please follow the instructions as below
# Run the server
`node server.js`

# Run the client
`node client.js [username] [password]`

## Send events
- Send broadcast message
`b;[message]`

- Send private message
`s;[username of receiver];[message]`

- List all users
`ls;`

- List all messages of a sender
`umsg;`

- Quit the chat
`q;`

- Join a group
`jg;[group name]`

- Send message to the group
`bg;[group name];[message]`

- List users in the group
`mbr;[group name]`

- List all messages in the group
`msg;[group name]`

- List all groups
`grp;`

- Leave the group
`lg;[group name]`

# Run unit tests
Run `npm test` at the root of project
