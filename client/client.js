const io = require("socket.io-client");
const socket = io("http://localhost:3000");
const crypto = require('crypto');
var nickname = null;

const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
const iv = crypto.randomBytes(16);

const encrypt = (text) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    };
};

console.log("Connecting to the server...");
socket.on("connect", () => {
    nickname = process.argv[2];
    password = process.argv[3];
    console.log("[INFO]: Welcome %s", nickname);
    socket.emit("join", { "sender": nickname, "action": "join", "password": password });
});
socket.on("disconnect", (reason) => {
    console.log("[INFO]: Server disconnected, reason: %s", reason);
    rl.close();
});

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on("line", (input) => {
    var cmd = input.split(";");
    if (true === input.startsWith("b;")) {
        var str = input.slice(2);
        socket.emit("broadcast", { "sender": nickname, "action": "broadcast", "msg": str });
    }
    else if ("ls;" === input) {
        socket.emit("list", { "sender": nickname, "action": "list" });
    }
    else if ("q;" === input) {
        socket.emit("quit", { "sender": nickname, "action": "quit" });
    }
    else if (true === input.startsWith("s;")) {
        socket.emit("send", { "sender": nickname, "receiver": cmd[1], "msg": encrypt(cmd[2]), "action": "send" });
    }
    else if (true === input.startsWith("jg;")) {
        socket.emit("join_group", { "sender": nickname, "group": cmd[1], "action": "join_group" });
    }
    else if (true === input.startsWith("lg;")) {
        socket.emit("leave_group", { "sender": nickname, "action": "leave_group", "group": cmd[1] });
    }
    else if (true === input.startsWith("bg;")) {
        socket.emit("broadcast_group", { "sender": nickname, "group": cmd[1], "msg": cmd[2], "action": "broadcast_group" });
    }
    else if (true === input.startsWith("mbr;")) {
        socket.emit("list_members_group", { "sender": nickname, "group": cmd[1], "action": "list_members_group" });
    }
    else if ("grp;" === input) {
        socket.emit("list_groups", { "sender": nickname, "action": "list_groups" });
    }
    else if (true === input.startsWith("msg;")) {
        socket.emit("list_messages_group", { "sender": nickname, "group": cmd[1], "action": "list_messages_group" });
    }
    else if ("umsg;" === input) {
        socket.emit("list_messages_user", { "sender": nickname, "action": "list_messages_user" });
    }
    else if ("tr;" === input) {
        socket.emit("trace");
    }
});

socket.on("broadcast", (data) => {
    console.log("[Broadcast message from %s]: %s", data.sender, data.msg);
});

socket.on("join", (data) => {
    console.log("[INFO]: %s has joined the chat", data.sender);
});

socket.on("list", (data) => {
    console.log("[INFO]: List of nicknames:");
    for (var i = 0; i < data.users.length; i++) {
        console.log(data.users[i]);
    }
});

socket.on("quit", (data) => {
    console.log("[INFO]: %s quit the chat", data.sender);
});

socket.on("send_receiver", (data) => {
    console.log("\x1b[33m[%s]: %s\x1b[0m", data.sender, data.msg);
});

socket.on("send_sender", (data) => {
    console.log("\x1b[31m[%s]: %s\x1b[0m", data.sender, data.msg);
});

socket.on("join_group", (data) => {
    console.log("%s joins the group %s", data.sender, data.group);
});

socket.on("leave_group", (data) => {
    console.log("%s left the group %s", data.sender, data.group);
});

socket.on("broadcast_group", (data) => {
    console.log("%s broadcasts the message [%s] in the group %s", data.sender, data.msg, data.group);
});

socket.on("list_members_group", (data) => {
    console.log("%s lists all clients that are inside the group %s", data.sender, data.group);
    for (var i = 0; i < data.users.length; i++) {
        console.log(data.users[i]);
    }
});

socket.on("list_groups", (data) => {
    console.log("[INFO]: List of existing groups:");
    for (var i = 0; i < data.groups.length; i++) {
        console.log(data.groups[i]);
    }
});

socket.on("list_messages_group", (data) => {
    console.log(data.message);
});

socket.on("list_messages_user", (data) => {
    console.log(data.message);
});
