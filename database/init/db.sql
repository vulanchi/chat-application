/* 
 *  Create & init site content
 */
DROP TABLE users;
DROP TABLE messages;
DROP TABLE accounts;

-- USERS --

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  name TEXT DEFAULT "",
  desc TEXT DEFAULT ""
);

-- ACCOUNTS --

CREATE TABLE accounts (
  id INTEGER PRIMARY KEY,
  username TEXT DEFAULT "",
  password TEXT DEFAULT ""
);

-- MESSAGES --

CREATE TABLE messages (
  id INTEGER PRIMARY KEY,
  sender TEXT DEFAULT "",
  content TEXT DEFAULT "",
  kind TEXT DEFAULT "",
  group_name TEXT DEFAULT "",
  reciever TEXT DEFAULT "",
  desc TEXT DEFAULT ""
);
