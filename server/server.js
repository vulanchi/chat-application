const port = 3000;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const io = require("socket.io")(port);
const sqlite3 = require('sqlite3').verbose();
console.log("Server is listening on port: %d", port);
var db = new sqlite3.Database('./../database/db.sqlite');
var groups = {};
const saltRounds = 10;

const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
const iv = crypto.randomBytes(16);

const decrypt = (hash) => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
    return decrpyted.toString();
};

io.on("connect", (socket) => {
    console.log("A user connected");

    socket.on("disconnect", (reason) => {
        console.log("A client disconnected, reason: %s", reason);
        console.log("Number of clients: %d", io.of('/').server.engine.clientsCount);
    });

    socket.on("broadcast", (data) => {
        console.log("%s", data);
        socket.broadcast.emit("broadcast", data);
        for (const [key, value] of io.of("/").sockets) {
            if (data.sender.toLowerCase() !== value.nickname) {
                db.serialize(function () {
                    var insertMessage = "INSERT INTO messages (sender, content, kind, reciever) VALUES ('" + data.sender + "', '" + data.msg + "', 'broadcast', '" + value.nickname + "');"
                    console.log(insertMessage);
                    db.run(insertMessage);
                });
            }
        }
    });

    socket.on("join", (data) => {
        console.log("Nickname: ", data.sender, ", ID: ", socket.id);
        console.log("Number of clients: %d", io.of('/').server.engine.clientsCount);
        var checkIfUserExists = "select count(*) from accounts where username = '" + data.sender + "'";
        db.get(checkIfUserExists, function (err, row) {
            if (err) {
                throw err;
            }
            if (row['count(*)'] !== 0) {
                console.log('Existing user: %s', data.sender);
                var getPassword = "select password from accounts where username = '" + data.sender + "'";
                db.get(getPassword, function (err, row) {
                    if (err) {
                        throw err;
                    }
                    bcrypt.compare(data.password, row['password'], function (err, result) {
                        if (err) {
                            throw err;
                        }
                        if (result === true) {
                            socket.nickname = data.sender;
                            socket.broadcast.emit("join", data);
                        } else {
                            console.log("Disconnect due to incorrect password");
                            socket.disconnect();
                        }
                    });
                });
            } else {
                bcrypt.hash(data.password, saltRounds, function (err, hash) {
                    if (err) {
                        throw err;
                    }
                    console.log('Adding new user to db');
                    var insertUser = "INSERT INTO accounts (username, password) VALUES ('" + data.sender + "', '" + hash + "');"
                    console.log(insertUser);
                    db.run(insertUser);
                    socket.nickname = data.sender;
                    socket.broadcast.emit("join", data);
                });

            }
        });
    });

    socket.on("list", (data) => {
        var users = [];
        for (const [key, value] of io.of("/").sockets) {
            users.push(value.nickname);
        }
        socket.emit("list", { "sender": data.sender, "action": "list", "users": users });
    });

    socket.on("quit", (data) => {
        console.log("\n%s", data);
        socket.broadcast.emit("quit", data);
        socket.disconnect(true);
    });

    socket.on("send", (data) => {
        msg = decrypt(data.msg)
        console.log("%s sends the message [%s] to %s", data.sender, msg, data.receiver);
        for (const [_, socket] of io.of("/").sockets) {
            if (data.receiver.toLowerCase() === socket.nickname) {
                socket.emit("send_receiver", { "sender": data.sender, receiver: data.receiver, "msg": msg, "action": "send" });
                db.serialize(function () {
                    var insertMessage = "INSERT INTO messages (sender, content, kind, reciever) VALUES ('" + data.sender + "', '" + msg + "', 'private', '" + data.receiver + "');"
                    console.log(insertMessage);
                    db.run(insertMessage);
                });
            }
        }
        socket.emit("send_sender", { "sender": data.sender, "receiver": data.receiver, "msg": msg, "action": "send" });
    });

    // Used for unittest to bypass encryption
    socket.on("send_test", (data) => {
        for (const [_, socket] of io.of("/").sockets) {
            if (data.receiver.toLowerCase() === socket.nickname) {
                socket.emit("send_receiver", { "sender": data.sender, receiver: data.receiver, "msg": data.msg, "action": "send" });
                db.serialize(function () {
                    var insertMessage = "INSERT INTO messages (sender, content, kind, reciever) VALUES ('" + data.sender + "', '" + data.msg + "', 'private', '" + data.receiver + "');"
                    console.log(insertMessage);
                    db.run(insertMessage);
                });
            }
        }
        socket.emit("send_sender", { "sender": data.sender, "receiver": data.receiver, "msg": data.msg, "action": "send" });
    });

    socket.on("join_group", (data) => {
        console.log("%s joins the group %s", data.sender, data.group);
        if (data.group in groups) {
            users = groups[data.group]["users"];
            users.push(data.sender.toLowerCase());
            groups[data.group]["users"] = users;
        }
        else {
            groups[data.group] = { "users": [data.sender.toLowerCase()] };
        }
        users = groups[data.group]["users"];
        for (const [_, socket] of io.of("/").sockets) {
            socket.emit("join_group", { "sender": data.sender, "group": data.group, "action": "join_group" });
        }
    });

    socket.on("leave_group", (data) => {
        console.log("\n%s", data);
        socket.leave(data.group);
        console.log("Group: ", data.group, ", Left: ", data.sender);
        for (const [_, socket] of io.of("/").sockets) {
            socket.emit("leave_group", { "sender": data.sender, "group": data.group, "action": "leave_group" });
        }
    });

    socket.on("broadcast_group", (data) => {
        console.log("%s broadcasts the message [%s] in the group %s", data.sender, data.msg, data.group);
        users = groups[data.group]["users"];
        db.serialize(function () {
            var insertMessage = "INSERT INTO messages (sender, content, kind, reciever, group_name) VALUES ('" + data.sender + "', '" + data.msg + "', 'broadcast_group', 'null', '" + data.group + "');"
            console.log(insertMessage);
            db.run(insertMessage);
        });
        for (const [_, socket] of io.of("/").sockets) {
            if (users.includes(`${socket.nickname}`)) {
                socket.emit("broadcast_group", { "sender": data.sender, "msg": data.msg, "group": data.group, "action": "broadcast_group" });
            }
        }
    });

    socket.on("list_members_group", (data) => {
        console.log("%s lists all clients that are inside the group %s", data.sender, data.group);
        users = groups[data.group]["users"];
        socket.emit("list_members_group", { "sender": data.sender, "users": users, "group": data.group, "action": "list_members_group" });
    });

    socket.on("list_groups", (data) => {
        console.log("%s lists the existing groups", data.sender);
        groups = Object.keys(groups);
        socket.emit("list_groups", { "sender": data.sender, "groups": groups, "action": "list_groups" });
    });

    socket.on("list_messages_group", (data) => {
        console.log("%s lists the history of messages exchanged in the group %s", data.sender, data.group);
        var retrieveMessages = "SELECT * FROM messages WHERE group_name = '" + data.group + "';"
        db.each(retrieveMessages, (err, row) => {
            if (err) {
                throw err;
            }
            socket.emit("list_messages_group", { "sender": data.sender, "group": data.group, "message": `[${row.sender}]: ${row.content}`, "action": "list_messages_group" });
        });
    });

    socket.on("list_messages_user", (data) => {
        console.log("%s lists all his / her messages", data.sender);
        var retrieveMessages = "SELECT * FROM messages WHERE sender = '" + data.sender + "';"
        db.each(retrieveMessages, (err, row) => {
            if (err) {
                throw err;
            }
            socket.emit("list_messages_user", { "message": `[${row.kind}]: ${row.content}`, "action": "list_messages_user" });
        });
    });

    // Listen to the 'trace' event emitted from the client
    socket.on("trace", () => {
        console.log("\n=============== Trace ===============");
        console.log(io.of("/"));
    });
});
