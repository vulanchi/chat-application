const io = require('socket.io-client');
const expect = require('chai').expect;

var num_tests = 1;

describe('Testing server in the chat', function () {

  var clien_1, clien_2;
  var user_1 = "stephen";
  var user_2 = "alan";
  var user_3 = "mimi";

  var options = { 'force new connection': true };

  beforeEach(function (done) {
    this.timeout(3000);

    console.log(">> Test #" + (num_tests++));

    clien_1 = io('http://localhost:3000', options);
    clien_2 = io('http://localhost:3000', options);
    clien_3 = io('http://localhost:3000', options);

    clien_1.on('connect', function () {
      clien_1.emit('join', { "sender": user_1, "password": "1", "action": "join" });
    });

    clien_2.on('connect', function () {
      clien_2.emit('join', { "sender": user_2, "password": "1", "action": "join" });
    });

    setTimeout(done, 500);
  });

  afterEach(function (done) {
    this.timeout(2000);

    clien_1.disconnect();
    clien_2.disconnect();

    setTimeout(done, 500);
  })

  it('Notify that a user joined the chat', function (done) {
    clien_1.on('join', function (data) {
      expect(data.sender).to.equal(user_2);
      done();
    });
  });

  it('Broadcast a message to others in the chat', function (done) {
    var msg = "Hi all!";
    clien_1.emit('broadcast', { "sender": user_1, "action": "broadcast", "msg": msg });
    clien_2.on('broadcast', function (data) {
      expect(data.msg).to.equal(msg);
      done();
    });
  });

  it('List all users in the chat', function (done) {
    clien_1.emit('list', { "sender": user_1, "action": "list" });
    clien_1.on('list', function (data) {
      expect(data.users).to.be.an('array').that.includes(user_2);
      done();
    });
  });

  it('Notify that a user quit the chat', function (done) {
    clien_3.on('connect', function () {
      clien_3.emit('join', { "sender": user_3, "password": "1", "action": "join" });
    });
    clien_3.emit('quit', { "sender": user_3, "action": "quit" });
    clien_1.on('quit', function (data) {
      expect(data.sender).to.equal(user_3);
      done();
    });
  });

  it('Send a private message to a user', function (done) {
    var msg = "Hi! This is a private message.";
    clien_1.emit('send_test', { "sender": user_1, "receiver": user_2, "action": "send", "msg": msg });
    clien_1.on('send_sender', function (data) {
      expect(data.receiver).to.equal(user_2);
      expect(data.msg).to.equal(msg);
      done();
    });
    clien_2.on('send_receiver', function (data) {
      expect(data.receiver).to.equal(user_2);
      expect(data.msg).to.equal(msg);
      done();
    });
  });
});

describe('Testing server in the group', function () {

  var clien_1, clien_2
  var user_1 = "jay";
  var user_2 = "mia";
  var group_name = "doctors";
  var options = { 'force new connection': true };

  beforeEach(function (done) {

    this.timeout(3000);

    console.log(">> Test #" + (num_tests++));

    clien_1 = io('http://localhost:3000', options);
    clien_2 = io('http://localhost:3000', options);

    clien_1.on('connect', function () {
      clien_1.emit('join', { "sender": user_1, "password": "1", "action": "join" });
      clien_1.emit('join_group', { "sender": user_1, "action": "join_group", "group": group_name });
    });

    clien_2.on('connect', function () {
      clien_2.emit('join', { "sender": user_2, "password": "1", "action": "join" });
      clien_2.emit('join_group', { "sender": user_2, "action": "join_group", "group": group_name });
    });

    setTimeout(done, 500);
  });

  afterEach(function (done) {
    this.timeout(2000);

    clien_1.disconnect();
    clien_2.disconnect();

    setTimeout(done, 500);
  })

  it('Notify that a user joined a group', function (done) {
    clien_1.on('join_group', function (data) {
      expect(data.sender).to.equal(user_1);
      expect(data.group).to.equal(group_name);
      done();
    });
  });

  it('Broadcast a message to a group', function (done) {
    var msg = "Say hello all to doctor group!";
    clien_1.emit('broadcast_group', { "sender": user_1, "group": group_name, "action": "broadcast_group", "msg": msg });
    clien_2.on('broadcast_group', function (data) {
      expect(data.group).to.equal(group_name);
      expect(data.msg).to.equal(msg);
      expect(data.sender).to.equal(user_1);
      done();
    });
  });

  it('List all clients that are inside a group', function (done) {
    clien_2.emit('join_group', { "sender": user_2, "action": "join_group", "group": group_name });
    clien_1.emit('list_members_group', { "sender": user_1, "group": group_name, "action": "list_members_group" });
    clien_1.on('list_members_group', function (data) {
      expect(data.group).to.equal(group_name);
      expect(data.users).to.be.an('array').that.includes(user_1);
      expect(data.users).to.be.an('array').that.includes(user_2);
      done();
    });
  });

  it('List the existing groups', function (done) {
    var new_group = "engineers";
    clien_2.emit('join_group', { "sender": user_2, "action": "join_group", "group": new_group });
    clien_1.emit('join_group', { "sender": user_1, "action": "join_group", "group": group_name });
    clien_1.emit('list_groups', { "sender": user_1, "action": "list_groups" });

    clien_1.on('list_groups', function (data) {
      expect(data.groups).to.be.an('array').that.includes(new_group);
      expect(data.groups).to.be.an('array').that.includes(group_name);
      done();
    });
  });

  it('List the existing groups', function (done) {
    var new_group = "engineers";
    clien_2.emit('join_group', { "sender": user_2, "action": "join_group", "group": new_group });
    clien_1.emit('join_group', { "sender": user_1, "action": "join_group", "group": group_name });
    clien_1.emit('list_groups', { "sender": user_1, "action": "list_groups" });

    clien_1.on('list_groups', function (data) {
      expect(data.groups).to.be.an('array').that.includes(new_group);
      expect(data.groups).to.be.an('array').that.includes(group_name);
      done();
    });
  });

  it('List the existing groups', function (done) {
    var new_group = "engineers";
    clien_2.emit('join_group', { "sender": user_2, "action": "join_group", "group": new_group });
    clien_1.emit('join_group', { "sender": user_1, "action": "join_group", "group": group_name });
    clien_1.emit('list_groups', { "sender": user_1, "action": "list_groups" });

    clien_1.on('list_groups', function (data) {
      expect(data.groups).to.be.an('array').that.includes(new_group);
      expect(data.groups).to.be.an('array').that.includes(group_name);
      done();
    });
  });
});
